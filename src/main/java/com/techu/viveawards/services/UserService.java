package com.techu.viveawards.services;

import com.techu.viveawards.models.AwardModel;
import com.techu.viveawards.models.ErrorModel;
import com.techu.viveawards.models.UserModel;
import com.techu.viveawards.repositories.UserRepository;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    // para GET usando findById
    public ObjectServiceResponse findById(String id) {
        System.out.println("findById");
        System.out.println("La id del empleado a buscar es: " + id);

        ObjectServiceResponse result = new ObjectServiceResponse();

        if(this.userRepository.findById(id).isPresent())
        {
            System.out.println("Usuario encontrado");
            result.setResponseHttpStatusCode(HttpStatus.OK);
            UserModel userModel = this.userRepository.findById(id).get();
            result.setUserModel(userModel);
        }
        else
        {
            System.out.println("ERROR: usuario no encontrado");
            ErrorModel errorModel = new ErrorModel();
            errorModel.setError("ERROR: Usuario no encontrado: "+id);
            result.setResponseHttpStatusCode(HttpStatus.NOT_FOUND);
            result.setError(errorModel);
        }

        return result;
    }

    // para GET usando findAll
    public List<UserModel> findAll() {
        return this.userRepository.findAll();
    }

    //para POST
    public UserModel add(UserModel user) {
        System.out.println("add");

        return this.userRepository.save(user);
    }

    //para PUT
    public UserModel update(UserModel user) {
        System.out.println("update");

        return this.userRepository.save(user);
    }


    //para DELETE
    public boolean delete(String id) {
        System.out.println("delete");

        boolean result = false;

        if (this.userRepository.findById(id).isPresent() == true) {
            System.out.println("He encontrado al empleado, borrando");
            this.userRepository.deleteById(id);

            result = true;
        }

        return result;
    }
}
