package com.techu.viveawards;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ViveawardsApplication {

	public static void main(String[] args) {
		SpringApplication.run(ViveawardsApplication.class, args);
	}

}
