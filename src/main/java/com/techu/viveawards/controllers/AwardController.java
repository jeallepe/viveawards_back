package com.techu.viveawards.controllers;


import com.techu.viveawards.models.AwardModel;
import com.techu.viveawards.services.AwardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/viveawards")
@CrossOrigin(origins = "*", methods = {RequestMethod.DELETE,RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT})
public class AwardController {

    @Autowired
    AwardService awardService;

    @PostMapping("/awards")
    public ResponseEntity<AwardModel> addUser(@RequestBody AwardModel award){
        System.out.println("addAward-Controlador");

        return new ResponseEntity<>(this.awardService.add(award), HttpStatus.CREATED);

    }

    @GetMapping("/awards")
    public ResponseEntity<List<AwardModel>> getAwards(){
        System.out.println("getAwards-Controlador");


        return new ResponseEntity<>(this.awardService.findAll(), HttpStatus.OK);

    }

    @GetMapping("/awards/{id}")
    public ResponseEntity<Object> getAwardstById (@PathVariable String id){
        System.out.println("getAwardstById");
        System.out.println("El id del premio a buscar es " + id);

        Optional<AwardModel> result = this.awardService.getById(id);

        if (result.isPresent() == true) {
            return new ResponseEntity<>(result.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Premio no encontrado", HttpStatus.NOT_FOUND);
        }

    }

    @DeleteMapping("/awards/{id}")
    public ResponseEntity<String> deleteAward(@PathVariable String id){
        System.out.println("deleteAward");
        System.out.println("El id del premio a borrar es " + id);

        boolean deletedAward = this.awardService.delete(id);

        return new ResponseEntity<>( deletedAward ? "Premio eliminado correctamente" : "Premio no encontrado",
                deletedAward ? HttpStatus.OK: HttpStatus.NOT_FOUND);

    }

}
