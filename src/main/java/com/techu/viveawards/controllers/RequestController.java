package com.techu.viveawards.controllers;

import com.techu.viveawards.models.RequestModel;
import com.techu.viveawards.services.RequestService;
import com.techu.viveawards.services.ObjectServiceResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/viveawards")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.DELETE, RequestMethod.POST, RequestMethod.PUT})
public class RequestController {

    @Autowired
    RequestService requestService;

    @GetMapping("/requests")
    public ResponseEntity<List<RequestModel>> getRequests()
    {
        System.out.println("<RequestController> getRequests");
        return new ResponseEntity<>(this.requestService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/requests/{id}")
    public ResponseEntity<Object> getRequestById(@PathVariable String id)
    {
        System.out.println("<RequestController> getRequestById");
        System.out.println("<RequestController> El id del request que vamos a obtener es "+id);

        Optional<RequestModel> result = this.requestService.findById(id);

        return new ResponseEntity<>(
                result.isPresent()? result.get() : "Request no encontrado"
                , result.isPresent()? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    @PostMapping("/requests")
    public ResponseEntity<Object>  addRequest(@RequestBody RequestModel request){
        System.out.println("<RequestController> addRequest");
        System.out.println("<RequestController> El userId del request que se va a crear es "+request.getUserId());
        System.out.println("<RequestController> El awardId del request que se va a crear es "+request.getAwardId());

        ObjectServiceResponse result = this.requestService.add(request);
        return new ResponseEntity<>(
                (result.getError()!=null)?result.getError():result.getAwardModel(), result.getResponseHttpStatusCode());
    }

    @DeleteMapping("/requests/{id}")
    public ResponseEntity<String> deleteRequest(@PathVariable String id){

        System.out.println("<RequestController> deleteRequest");
        System.out.println("<RequestController> El id del request que vamos a borrar es "+id);

        boolean deleteRequest = this.requestService.delete(id);

        return new ResponseEntity<>(
                deleteRequest ? "Request borrado" : "Request no borrado",
                deleteRequest ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @PutMapping("/requests/{id}")
    public ResponseEntity<RequestModel> updateRequest(@RequestBody RequestModel request, @PathVariable String id){

        System.out.println("<RequestController> updateRequest");
        System.out.println("<RequestController> El id del request que vamos a actualizar es "+id);
        System.out.println("<RequestController> El userdId del request que se va a actualizar  es "+request.getUserId());
        System.out.println("<RequestController> El awardId del request que se va a actualizar es "+request.getAwardId());

        Optional<RequestModel> requestToUpdate = this.requestService.findById(id);

        if(requestToUpdate.isPresent()){

            System.out.println("<RequestController> Request para actualizar encontrado, actualizando");

            this.requestService.update(request);
        }

        return new ResponseEntity<>(request,
                requestToUpdate.isPresent()? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

}
