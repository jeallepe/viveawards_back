package com.techu.viveawards.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/viveawards")
public class HelloController {

    @RequestMapping("/")
    public ResponseEntity index() {
        System.out.println("Hello Controller");
        return new ResponseEntity<>("Hola Mundo desde API Tech U!", HttpStatus.OK);
        //return ResponseEntity.ok("Hola Mundo");
    }

    @RequestMapping("/hello")
    public String hello(@RequestParam(value = "name", defaultValue = "TechU") String name){
        return "Hola Controller: "+name;
    }
}
