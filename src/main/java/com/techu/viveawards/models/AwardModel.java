package com.techu.viveawards.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "awards")  //nombre de tabla
public class AwardModel {

    @Id
    private String id;
    private String awards_name;
    private String awards_desc;
    private Integer puntos;

    public AwardModel() {
    }

    public AwardModel(String id, String awards_name, String awards_desc, Integer puntos) {
        this.id = id;
        this.awards_name = awards_name;
        this.awards_desc = awards_desc;
        this.puntos = puntos;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAwards_name() {
        return awards_name;
    }

    public void setAwards_name(String awards_name) {
        this.awards_name = awards_name;
    }

    public String getAwards_desc() {
        return awards_desc;
    }

    public void setAwards_desc(String awards_desc) {
        this.awards_desc = awards_desc;
    }

    public Integer getPuntos() {
        return puntos;
    }

    public void setPuntos(Integer puntos) {
        this.puntos = puntos;
    }
}
